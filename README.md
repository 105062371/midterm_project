# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : [網頁拍賣]
* Key functions (add/delete)
    1. [xxxx]
    
* Other functions (add/delete)
    1. [xxxx]
    2. [xxxx]
    3. [xxxx]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：[https://software-studio-ac892.firebaseapp.com/]

# Components Description : 
## Membership Mechanism (15%)
– Sign IN/Up/out:
![](https://i.imgur.com/32idAMp.png)

----
![](https://i.imgur.com/K1QFqea.png)

---
![](https://i.imgur.com/rC2KnQX.png)

---
![](https://i.imgur.com/2bJxH1q.png)




## Host on your Firebase page (5%)
Use Firebase deploy to host <font color="red">done</font>

working fine
## Database read/write (15%)
– Read/Write your membership data, or other usefull data in
authenticated way
In the file **./public/commodity/buy.js** & **./public/js/selling.js** you can see that I have Read/Write my database correctly
![](https://i.imgur.com/iFPx7uB.png)

---
![](https://i.imgur.com/rNpnYgz.png)

## or you can just go to the web site and login to buy something
* click the item
![](https://i.imgur.com/uuL1xcH.png)
* buy something
![](https://i.imgur.com/Au58eHB.png)
* go to the personal profile page
![](https://i.imgur.com/5xxtsRA.png)
* click the red circle
![](https://i.imgur.com/vQs9qyc.png)
* The newest bought item would display at very below
![](https://i.imgur.com/xzRYag1.png)

## and we can also sell thing 
* Go to profile page and enter the object name you want to sell and click "sell something" button
![](https://i.imgur.com/reQXMTy.png)
* go back to index page you will see your own item add on the shopping page
![](https://i.imgur.com/6wlYGjz.png)
* you can click the 買買買 button to buy this
![](https://i.imgur.com/hzikJtD.png)
* Here, we demo with another account "12345678@gmail.com".First, buy the stuff and go to profile to check whether we have bought it
![](https://i.imgur.com/42lSt86.png)
* Success!! 
![](https://i.imgur.com/zWxqITX.png)


##  RWD (15%)
– Check your website working fine on different size device
* The object will be squeezed under if we change the screen
![](https://i.imgur.com/e3d80h6.png)

* And you can also switch to the manga category
![](https://i.imgur.com/pRv6ehA.png)
![](https://i.imgur.com/O9FQxzD.png)

## Sign Up/In with Google or other third-party accounts (2.5%)
![](https://i.imgur.com/th5kA10.png)


## Add Chrome notification (5%)
![](https://i.imgur.com/yU4isml.png)

## Use CSS animation (2.5%)
You can hover on the item picture
## Prove your website has strong security (write in your report) (5%)
* I have two list "product" & "seller_list". I lock the "seller_list" as showed below that database will check person who modified the data is current user or not
![](https://i.imgur.com/a2SzNbU.png)
![](https://i.imgur.com/91BCyFb.png)


• Other functions not mentioned above (1~10%)
# Other Functions Description(1~10%) : 
1. [xxxx] : [xxxx]
2. [xxxx] : [xxxx]
3. [xxxx] : [xxxx]
...

## Security Report (Optional)
